window.onload = function () {
    function formatDate(date) {
        return (
            date.getDate() +
            "/" +
            (date.getMonth() + 1) +
            "/" +
            date.getFullYear()
        );
    }


    let currentDate = formatDate(new Date());

    $("#datetimepicker").datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true,
        startDate: currentDate,
        orientation: "bottom right"
    });

    $(".form-check-input").on("change", async function () {
        let data = new FormData();
        data.append('id', $(this).attr("id"));
        data.append('is_done', $(this).is(':checked'));
        data.append(
            'csrfmiddlewaretoken',
            $('input[name="csrfmiddlewaretoken"]').attr('value')
        );
        await fetch('/todo-change/', {
            method: 'POST',
            body: data
        })
        location.reload()
    })

    $(".todo").dblclick(function () {
        let todo = $(this).attr("id")
        let todo_id = todo.split("-")[1]
        let input_text = $(`#input-text-${todo_id}`)
        if (input_text.prop("disabled")) {
            input_text.prop("disabled", false)
        } else {
            input_text.prop("disabled", true)
        }
    })

    $(".btn-primary").click(async function () {
        let id = $(this).attr("id").slice(5)
        let data = new FormData();
        data.append("id", id)
        data.append(
            'csrfmiddlewaretoken',
            $('input[name="csrfmiddlewaretoken"]').attr('value')
        );
        data.append("title", $(`#input-text-${id}`).val())
        await fetch('/edit/', {
            method: 'POST',
            body: data
        })
        location.reload()
    })

    $(".btn-danger").click(async function () {
        let id = $(this).attr("id").slice(7)
        let data = new FormData();
        data.append("id", id)
        data.append(
            'csrfmiddlewaretoken',
            $('input[name="csrfmiddlewaretoken"]').attr('value')
        );
        await fetch('/delete/', {
            method: 'POST',
            body: data
        })
        location.reload()
    })

    $("#filter-select").on("change", async function () {
        window.location.href = $(this).val()
    })

    let filter = window.location.href.split("/")[4]
    if (filter){
        $(`#filter-select option[value=${filter}]`).prop('selected', true)
    }

    $("#sort-select").on("change", async function () {
        window.location.href = $(this).val()
    })

    let sort = window.location.href.split("/").slice(-1)
    if (sort[0]){
        $(`#sort-select option[value=${sort}]`).prop('selected', true)
    }
};
