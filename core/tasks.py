from django.utils import timezone

from core.enums import Status
from djangoTodo.celery import app
from core.models import Todo


@app.task
def check_beat_todo():
    todos = Todo.objects.all()
    for todo in todos:
        if todo.deadline < timezone.now().date():
            todo.status = Status(2).name

        if 0 <= (todo.deadline - timezone.now().date()).days < 2:
            todo.status = Status(1).name
        todo.save()
