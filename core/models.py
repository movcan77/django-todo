from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class Todo(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    deadline = models.DateField()
    is_done = models.BooleanField(default=False)
    status = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return f"{self.title}"
