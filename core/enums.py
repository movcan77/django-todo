from enum import Enum


class Status(Enum):
    HAS_DUE_DATE = 1
    OVERDUE = 2
    ACTIVE = 3
