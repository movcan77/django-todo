from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.views.generic import ListView, TemplateView, FormView

from core.enums import Status
from core.forms import RegistrationForm
from core.models import Todo
import dateutil.parser


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/login/'

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)


class DashboardView(LoginRequiredMixin, ListView):
    template_name = "dashboard.html"
    model = Todo

    def get_queryset(self):
        return Todo.objects.filter(user=self.request.user)


class CreateTodoView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def post(self, request):
        title = request.POST.get("title")
        deadline = dateutil.parser.parse(request.POST.get("deadline"))
        Todo.objects.create(
            title=title,
            deadline=deadline,
            user=request.user,
            is_done=False,
            status=Status(3).value
        )
        return redirect("/dashboard")


class DoneTodoView(LoginRequiredMixin, ListView):
    template_name = "includes/todos.html"
    model = Todo

    def post(self, request):
        todo_id = request.POST.get("id").split("-")[-1]
        is_done = request.POST.get('is_done') == 'true'
        todo = Todo.objects.get(id=todo_id)
        todo.is_done = is_done
        todo.save()
        return redirect("/dashboard")


class TodoEditView(LoginRequiredMixin, ListView):
    template_name = "dashboard.html"
    model = Todo

    def post(self, request):
        todo_id = request.POST.get("id")
        title = request.POST.get("title")
        todo = Todo.objects.get(id=todo_id)
        todo.title = title
        todo.save()
        return redirect("/dashboard")


class TodoDeleteView(LoginRequiredMixin, ListView):
    template_name = "dashboard.html"
    model = Todo

    def post(self, request):
        todo_id = request.POST.get("id")
        todo = Todo.objects.get(id=todo_id)
        todo.delete()
        return redirect("/dashboard")


class FilterView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(FilterView, self).get_context_data(**kwargs)
        if kwargs.get("status") == "completed":
            context.update({
                "object_list": Todo.objects.filter(is_done=True)
            })
        if kwargs.get("status") == "active":
            context.update({
                "object_list": Todo.objects.filter(is_done=False)
            })
        if kwargs.get("status") == "has_due_date":
            context.update({
                "object_list": Todo.objects.filter(status=Status(1).name)
            })
        if kwargs.get("status") == "overdue":
            context.update({
                "object_list": Todo.objects.filter(status=Status(2).name)
            })
        if kwargs.get("status") == "all":
            context.update({
                "object_list": Todo.objects.all()
            })
        if kwargs.get("status") == "due-date-desc":
            context.update({
                "object_list": Todo.objects.all().order_by("-deadline")
            })
        if kwargs.get("status") == "due-date-asc":
            context.update({
                "object_list": Todo.objects.all().order_by("deadline")
            })
        return context



