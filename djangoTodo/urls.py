"""djangoTodo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LogoutView, LoginView
from django.urls import path, include

from core import views
from core.views import DashboardView, CreateTodoView, DoneTodoView, TodoEditView, TodoDeleteView, FilterView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.RegistrationView.as_view()),
    path('accounts/', include('allauth.urls')),
    path('login/', LoginView.as_view(template_name='login.html', )),
    path('dashboard/', DashboardView.as_view()),
    path('add-todo/', CreateTodoView.as_view()),
    path('todo-change/', DoneTodoView.as_view()),
    path('logout', LogoutView.as_view()),
    path('edit/', TodoEditView.as_view()),
    path('delete/', TodoDeleteView.as_view()),
    path('dashboard/<str:status>', FilterView.as_view()),
]
